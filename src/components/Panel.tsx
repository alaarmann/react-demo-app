import React from "react";

export function Panel({
  title,
  children,
  className,
  actionBar
}: {
  title: string;
  children: React.ReactNode;
  className?: string;
  actionBar?: React.ReactNode;
}) {
  return (
    <div className={className}>
      <h2>{title}</h2>
      {children}
      {actionBar && <div className="Panel-ActionBar">{actionBar}</div>}
    </div>
  );
}
