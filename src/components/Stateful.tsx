import React, { useState, useEffect, useCallback } from "react";
import { User, UserModel } from "./User";
import { TodoList, TodoModel } from "./TodoList";
import { Panel } from "./Panel";

const ID_OFFSET = 100;
export function Stateful() {
  console.log("Rendering Stateful");
  const [value, setValue] = useState("A string value");
  const [user, setUser] = useState<UserModel>({ name: "abrakad", age: 53 });
  const [todoList, setTodoList] = useState<TodoModel[]>([
    {
      id: ID_OFFSET + 1,
      title: "Todo 1",
      description: "First thing to do"
    }
  ]);

  useEffect(() => {
    console.log("Did mount Stateful");
    return () => console.log("Did unmount Stateful");
  }, []);

  const mutateState = () => {
    const mutatedUser = user;
    mutatedUser.name += "_mutated";
    setUser(mutatedUser);

    const mutatedTodoList = todoList;
    mutatedTodoList.push({
      id: ID_OFFSET + mutatedTodoList.length + 1,
      title: `Todo ${mutatedTodoList.length + 1}`,
      description: "Added by list mutation"
    });
    setTodoList(mutatedTodoList);
  };

  const immutablyChangeState = () => {
    setUser({ ...user, name: user.name + "_immutablyChanged" });

    setTodoList([
      ...todoList,
      {
        id: ID_OFFSET + todoList.length + 1,
        title: `Todo ${todoList.length + 1}`,
        description: "Added by immutable change"
      }
    ]);
  };

  const changeValue = () => setValue(value + "_immutablyChanged");

  const changeTitle = useCallback(
    (id: number, title: string) =>
      setTodoList(currentList =>
        currentList.map(item => (item.id === id ? { ...item, title } : item))
      ),
    [setTodoList]
  );

  const changeDescription = useCallback(
    (id: number, description: string) =>
      setTodoList(currentList =>
        currentList.map(item =>
          item.id === id ? { ...item, description } : item
        )
      ),
    [setTodoList]
  );

  const emphasizeDescriptions = useCallback(() => {
    setTodoList(currentList =>
      currentList.map(item => ({
        ...item,
        description: `*${item.description}*`
      }))
    );
  }, [setTodoList]);

  const actionBar = (
    <>
      <button onClick={mutateState}>Mutate state (wrong way)</button>
      <button onClick={immutablyChangeState}>
        Change state immutably (only way)
      </button>
      <button onClick={changeValue}>Change Value</button>
      <button onClick={emphasizeDescriptions}>
        Emphasize Todo descriptions (not working, why?)
      </button>
    </>
  );

  return (
    <Panel
      title="Stateful Component"
      className="Stateful"
      actionBar={actionBar}
    >
      <div>{`Value: ${value}`}</div>
      <User name={user.name} age={user.age} />
      <TodoList
        todoList={todoList}
        changeTitle={changeTitle}
        changeDescription={changeDescription}
      />
    </Panel>
  );
}
