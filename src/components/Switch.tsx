import React, { useState, useCallback } from "react";
import { Stateful } from "./Stateful";
import { StatefulClassy } from "./StatefulClassy";

export function Switch() {
  const [viewToggle, setViewToggle] = useState<boolean>(true);

  const switchView = useCallback(() => setViewToggle(value => !value), [
    setViewToggle
  ]);

  return (
    <div className="Switch">
      {viewToggle ? <Stateful /> : <StatefulClassy />}
      <button onClick={switchView}>Switch to other view</button>
    </div>
  );
}
