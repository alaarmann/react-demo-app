import React, { useState, useCallback } from "react";
import { Panel } from "./Panel";

type Props = {
  left: React.ReactNode;
  middle: React.ReactNode;
  right: React.ReactNode;
};
export function ReusableSwitch(props: Props) {
  const { left, middle, right } = props;
  const [view, setView] = useState<number>(0);

  const switchToNextView = useCallback(
    () => setView(value => (value + 1) % 3),
    [setView]
  );
  const actionBar = (
    <div className="Switch-Actions">
      <button onClick={switchToNextView}>Switch to next view</button>
      <div className="Switch-CurrentPosition">
        <input
          type="radio"
          name="view"
          value="left"
          checked={view === 0}
          readOnly
        />
        <input
          type="radio"
          name="view"
          value="middle"
          checked={view === 1}
          readOnly
        />
        <input
          type="radio"
          name="view"
          value="right"
          checked={view === 2}
          readOnly
        />
      </div>
    </div>
  );
  return (
    <Panel title="Switch" className="Switch" actionBar={actionBar}>
      {view === 0 && left}
      {view === 1 && middle}
      {view === 2 && right}
    </Panel>
  );
}
