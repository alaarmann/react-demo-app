import React from "react";
import { Panel } from "./Panel";

export type UserModel = {
  name: string;
  age: number;
};

export function User(props: UserModel) {
  const { name, age } = props;
  return (
    <Panel title="User" className="User">
      <div>{`Name: ${name}`}</div>
      <div>{`Age: ${age}`}</div>
    </Panel>
  );
}
