import React, { Component } from "react";
import { User, UserModel } from "./User";
import { TodoList, TodoModel } from "./TodoList";
import { Panel } from "./Panel";

type Props = {
  name?: string;
};

type State = {
  value: string;
  user: UserModel;
  todoList: TodoModel[];
};

const ID_OFFSET = 100;
export class StatefulClassy extends Component<Props, State> {
  state = {
    value: "A string value",
    user: { name: "abrakad", age: 53 },
    todoList: [
      {
        id: ID_OFFSET + 1,
        title: "Todo 1",
        description: "First thing to do"
      }
    ]
  };

  componentDidMount() {
    console.log("Did mount StatefulClassy");
  }

  componentWillUnmount() {
    console.log("Did unmount StatefulClassy");
  }

  immutablyChangeState = () => {
    const { user, todoList } = this.state;

    this.setState(() => ({
      user: { ...user, name: user.name + "_immutablyChanged" }
    }));

    this.setState(() => ({
      todoList: [
        ...todoList,
        {
          id: ID_OFFSET + todoList.length + 1,
          title: `Todo ${todoList.length + 1}`,
          description: "Added by immutable change"
        }
      ]
    }));
  };

  changeValue = () =>
    this.setState(() => ({ value: this.state.value + "_immutablyChanged" }));

  changeTitle = (id: number, title: string) =>
    this.setState(currentState => ({
      todoList: currentState.todoList.map(item =>
        item.id === id ? { ...item, title } : item
      )
    }));

  changeDescription = (id: number, description: string) =>
    this.setState(currentState => ({
      todoList: currentState.todoList.map(item =>
        item.id === id ? { ...item, description } : item
      )
    }));

  render = () => {
    const { name } = this.props;
    const { value, user, todoList } = this.state;
    console.log("Rendering StatefulClassy");
    const actionBar = (
      <>
        <button onClick={this.immutablyChangeState}>
          Change state immutably (only way)
        </button>
        <button onClick={this.changeValue}>Change value</button>
      </>
    );
    return (
      <Panel
        title={name ? name : "Stateful Class-Based Component"}
        className="Stateful"
        actionBar={actionBar}
      >
        <div>{`Value: ${value}`}</div>
        <User name={user.name} age={user.age} />
        <TodoList
          todoList={todoList}
          changeTitle={this.changeTitle}
          changeDescription={this.changeDescription}
        />
      </Panel>
    );
  };
}
