// pure functions

export function add(one: number, other: number) {
  return one + other;
}

export function convertToUpperCase(arg: string) {
  return arg.toUpperCase();
}

export function incrementArray(arg: number[]) {
  return arg.map(item => add(item, 1));
}

// impure functions, with (side) effect

export function getCurrentDate() {
  return new Date();
}

export function getRandomValue() {
  return Math.random();
}

export function setDocumentTitle(title: string) {
  document.title = title;
}

export function mutatingFunction(person: { name: string; age: number }) {
  person.name = person.name.toUpperCase();
  return person;
}

export function throwingFunction() {
  throw new Error();
}

export function divide(dividend: number, divisor: number) {
  return dividend / divisor;
}
