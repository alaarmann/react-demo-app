describe("Equality", () => {
  test("with objects", () => {
    const obj1 = {};
    const obj2 = obj1;

    expect(obj1 === obj2).toBeTruthy();
  });

  test("even with mutated objects", () => {
    const obj1 = { name: "Carl" };
    const obj2 = obj1;

    console.log("obj1 before mutation", obj1);
    obj1.name = "Linda";
    console.log("obj2", obj2);

    expect(obj1 === obj2).toBeTruthy();
  });

  test("with arrays", () => {
    const arr1: any[] = [];
    const arr2 = arr1;

    expect(arr1 === arr2).toBeTruthy();
  });

  test("even with mutated arrays", () => {
    const arr1: string[] = ["abc", "def"];
    const arr2 = arr1;

    console.log("arr1 before mutation", arr1);
    arr1.push("ghi");
    console.log("arr2", arr2);

    expect(arr1 === arr2).toBeTruthy();
  });
});

describe("Inequality", () => {
  test("with objects", () => {
    const obj1 = { name: "Carl" };
    const obj2 = { name: "Carl" };

    expect(obj1 === obj2).toBeFalsy();
  });

  test("with arrays", () => {
    const arr1: string[] = ["abc", "def"];
    const arr2: string[] = ["abc", "def"];

    expect(arr1 === arr2).toBeFalsy();
  });

  test("with functions", () => {
    const fun1: (arg: number) => number = (arg: number) => arg + 1;
    const fun2: (arg: number) => number = (arg: number) => arg + 1;

    expect(fun1 === fun2).toBeFalsy();
  });
});

export default {};
