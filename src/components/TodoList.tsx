import React, { useCallback, useState } from "react";
import { Panel } from "./Panel";

export type TodoModel = {
  id: number;
  title: string;
  description: string;
};

type Props = {
  todoList: TodoModel[];
  changeTitle: (id: number, title: string) => void;
  changeDescription: (id: number, description: string) => void;
};

export function TodoList(props: Props) {
  const { todoList, changeTitle, changeDescription } = props;
  return (
    <Panel title="Todo List" className="TodoList">
      <ul className="TodoList-List">
        {todoList.map(({ id, title, description }) => (
          <Todo
            key={id}
            id={id}
            title={title}
            description={description}
            changeTitle={changeTitle}
            changeDescription={changeDescription}
          />
        ))}
      </ul>
    </Panel>
  );
}

function Todo(
  props: TodoModel & {
    changeTitle: (id: number, title: string) => void;
    changeDescription: (id: number, description: string) => void;
  }
) {
  const { id, title, description, changeTitle, changeDescription } = props;
  const [descriptionValue, setDescriptionValue] = useState(description);

  const onChangeTitle = useCallback(
    event => changeTitle(id, event.target.value),
    [id, changeTitle]
  );

  const [descriptionModified, setDescriptionModified] = useState(false);

  const onChangeDescription = useCallback(
    event => {
      setDescriptionValue(event.target.value);
      setDescriptionModified(true);
    },
    [setDescriptionValue, setDescriptionModified]
  );

  const onKeyPressDescription = useCallback(
    event => {
      if (event.key === "Enter") {
        changeDescription(id, descriptionValue);
        setDescriptionModified(false);
      }
    },
    [changeDescription, id, descriptionValue, setDescriptionModified]
  );

  return (
    <li className="TodoList-ListItem">
      <span className="TodoList-ListItemId">{`Id '${id}'`}</span>
      <label className="TodoList-ListItemTitle">
        Title:
        <input type="text" value={title} onChange={onChangeTitle} />
      </label>
      <label
        className={
          "TodoList-ListItemDescription " +
          (descriptionModified
            ? "TodoList-ListItemDescription-modified"
            : "TodoList-ListItemDescription-unmodified")
        }
      >
        Description:
        <input
          type="textarea"
          value={descriptionValue}
          onChange={onChangeDescription}
          onKeyPress={onKeyPressDescription}
        />
      </label>
    </li>
  );
}
