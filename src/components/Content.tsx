import React from "react";
import { ReusableSwitch } from "./ReusableSwitch";
import { Stateful } from "./Stateful";
import { StatefulClassy } from "./StatefulClassy";
import { Panel } from "./Panel";

export function Content() {
  const left = <Stateful />;
  const middle = (
    <StatefulClassy key="a" name="Stateful Class-Based Component #1" />
  );
  const right = (
    <StatefulClassy key="a" name="Stateful Class-Based Component #2" />
  );
  return (
    <Panel title="Content" className="Content">
      <ReusableSwitch left={left} middle={middle} right={right} />
    </Panel>
  );
}
